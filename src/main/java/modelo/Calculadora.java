/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELIAS
 */
public class Calculadora {

    private int capital;
    private double interes;
    private int cantaños;
    private double resultado;
    
    
    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(int interes) {
        this.interes = interes;
    }

    /**
     * @return the cantaños
     */
    public int getCantaños() {
        return cantaños;
    }

    /**
     * @param cantaños the cantaños to set
     */
    public void setCantaños(int cantaños) {
        this.cantaños = cantaños;
    }

    /**
     * @return the resultado
     */
    public double getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
    public void calcular(int capital,double interes,int cantaños){
        
        this.resultado=capital*interes*cantaños;
                
        
    }
    
    
    
    
}
